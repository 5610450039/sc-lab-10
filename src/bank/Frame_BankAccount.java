package bank;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;



public class Frame_BankAccount extends JFrame{
	private static final int FRAME_WIDTH = 250;
	private static final int FRAME_HEIGHT = 250;
	private BankAccount bankAcc;
	private JPanel panel,top,center,bottom;
	private JTextField textDepo,textWith;
	private JButton buttonDepo,buttonWith;
	private JLabel labelBal;
	
	public static void main(String[] args){
		new Frame_BankAccount();
	}
	
	public Frame_BankAccount(){
		createPanel();
		setSize(FRAME_WIDTH,FRAME_HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setLocation(600,200);
		bankAcc = new BankAccount();
		
		
	}


	private void createPanel() {
		panel = new JPanel();
		top = new JPanel();
		center = new JPanel();
		bottom = new JPanel();
		textDepo = new JTextField(7);
		textWith = new JTextField(7);
		labelBal = new JLabel("Please fill the information.");
		buttonDepo = new JButton("Comfirm");
		buttonWith = new JButton("Comfirm");
		top.add(textDepo);
		top.add(buttonDepo);
		center.add(textWith);
		center.add(buttonWith);
		bottom.add(labelBal);


		top.setBorder(new TitledBorder(new EtchedBorder(),"Deposit"));
		center.setBorder(new TitledBorder(new EtchedBorder(),"Withdraw"));
		bottom.setBorder(new TitledBorder(new EtchedBorder(),"Balance"));
		panel.add(top);
		panel.add(center);
		panel.add(bottom);
		panel.setLayout(new GridLayout(3,1));
		add(panel,BorderLayout.CENTER);
		
		buttonDepo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				bankAcc.deposit(Integer.parseInt(textDepo.getText()));
				setBalanceText(bankAcc.toString());
				clearButtonText();
			}
		});
		buttonWith.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(bankAcc.getBalance()<Integer.parseInt(textWith.getText())){
					setBalanceText("Not enough money, please try again.");
					clearButtonText();
				}
				else{
					bankAcc.withdraw(Integer.parseInt(textWith.getText()));
					setBalanceText(bankAcc.toString());
					clearButtonText();
				}
			}
		});
			
			
	}
	public void setBalanceText(String str){
		labelBal.setText(str);
		
	}
	public void clearButtonText(){
		textDepo.setText("");
		textWith.setText("");
		
	}
		
		

}
