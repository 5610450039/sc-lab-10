package bank;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;



public class Frame_BankAccountTextArea extends JFrame{
	private static final int FRAME_WIDTH = 465;
	private static final int FRAME_HEIGHT = 200;
	private BankAccount bankAcc;
	private JPanel panelAll,panel,top,center;
	private JTextField textDepo,textWith;
	private JButton buttonDepo,buttonWith;
	private JTextArea textArea;
	private JScrollPane scroll;
	private String historyText = "";
	
	public static void main(String[] args){
		new Frame_BankAccountTextArea();
	}
	
	public Frame_BankAccountTextArea(){
		createPanel();
		setSize(FRAME_WIDTH,FRAME_HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setLocation(600,200);
		bankAcc = new BankAccount();

	}


	private void createPanel() {
		panel = new JPanel();
		top = new JPanel();
		center = new JPanel();
		textDepo = new JTextField(7);
		textWith = new JTextField(7);
		buttonDepo = new JButton("Comfirm");
		buttonWith = new JButton("Comfirm");
		top.add(textDepo);
		top.add(buttonDepo);
		center.add(textWith);
		center.add(buttonWith);
		
		historyText = "Welcome to a Bank.\n";
		textArea = new JTextArea(historyText);
		scroll = new JScrollPane(textArea);
		panelAll = new JPanel();
		

		top.setBorder(new TitledBorder(new EtchedBorder(),"Deposit"));
		center.setBorder(new TitledBorder(new EtchedBorder(),"Withdraw"));
		panel.add(top);
		panel.add(center);
		//panel.add(bottom);
		panel.setLayout(new GridLayout(2,1));
		
		
		panelAll.add(panel);
		panelAll.add(scroll);
		panelAll.setLayout(new GridLayout(1,2));
		
		add(panelAll,BorderLayout.CENTER);
		
		buttonDepo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				bankAcc.deposit(Integer.parseInt(textDepo.getText()));
				setDepoText(String.valueOf(bankAcc.getBalance()));
				clearButtonText();
			}
		});
		buttonWith.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(bankAcc.getBalance()<Integer.parseInt(textWith.getText())){
					setErrorText();
					clearButtonText();
				}
				else{
					bankAcc.withdraw(Integer.parseInt(textWith.getText()));
					setWithText(String.valueOf(bankAcc.getBalance()));
					clearButtonText();
				}
			}
		});
			
	}
	public void setDepoText(String str){
		historyText+="Deposit : "+textDepo.getText()+"\n-Your balance is "+str+"\n";
		textArea.setText(historyText);
	}
	public void setWithText(String str){
		historyText+="Withdraw : "+textWith.getText()+"\n-Your balance is "+str+"\n";
		textArea.setText(historyText);
	}
	public void setErrorText(){
		historyText+="Not enough money, please try again.\n";
		textArea.setText(historyText);
	}
	public void clearButtonText(){
		textDepo.setText("");
		textWith.setText("");
		
	}
		
		

}
