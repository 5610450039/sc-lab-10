package bank;

public class BankAccount {
	private double balance;
	
	
	public void deposit(double money){
		this.balance = this.balance+money;
	}
	
	public void withdraw(double money){
		this.balance = this.balance-money;
	}
	
	
	public double getBalance(){
		return balance;
	}
	
	public String toString(){
		return "Your balance is "+this.balance;
	}

}
