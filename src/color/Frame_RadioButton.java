package color;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;



public class Frame_RadioButton extends JFrame{
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 335;
	private JPanel colorPanel,RadioB;
	private JRadioButton red,green,blue;
	
	public static void main(String[] args){
		new Frame_RadioButton();
	}
		
	public Frame_RadioButton(){
		setSize(FRAME_WIDTH,FRAME_HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setLocation(600,200);
		createPanel();
		
	}
	private void createPanel() {
		colorPanel = new JPanel();
		colorPanel.setBackground(new Color(255,0,0));
		add(colorPanel,BorderLayout.CENTER);
		
		red = new JRadioButton("Red");
		green = new JRadioButton("Green");
		blue = new JRadioButton("Blue");
		red.setSelected(true);
		RadioB = new JPanel();
		RadioB.add(red);
		RadioB.add(green);
		RadioB.add(blue);
		add(RadioB,BorderLayout.SOUTH);
		
		ButtonGroup bGroup = new ButtonGroup();
		bGroup.add(red);
		bGroup.add(green);
		bGroup.add(blue);
		
		class redListener implements ActionListener{
			public void actionPerformed(ActionEvent e){
				colorPanel.setBackground(new Color(255,0,0));
			}
		}
		class greenListener implements ActionListener{
			public void actionPerformed(ActionEvent e){
				colorPanel.setBackground(new Color(0,255,0));
			}
		}
		class blueListener implements ActionListener{
			public void actionPerformed(ActionEvent e){
				colorPanel.setBackground(new Color(0,0,255));
			}
		}
	
		red.addActionListener(new redListener());
		green.addActionListener(new greenListener());
		blue.addActionListener(new blueListener());
	}
	
}
	


