package color;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class Frame_CheckBox extends JFrame{
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 335;
	private JPanel colorPanel,panel;
	private JCheckBox red,green,blue;
	private ActionListener aList;
	
	public static void main(String[] args){
		new Frame_CheckBox();
	}
		
	public Frame_CheckBox(){
		setSize(FRAME_WIDTH,FRAME_HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setLocation(600,200);
		createPanel();
		aList = new addActionListener();
		red.addActionListener(aList);
		green.addActionListener(aList);
		blue.addActionListener(aList);
		
	}
	private void createPanel() {
		colorPanel = new JPanel();
		colorPanel.setBackground(new Color(0,0,0));
		add(colorPanel,BorderLayout.CENTER);
		
		panel = new JPanel();
		red = new JCheckBox("Red");
		green = new JCheckBox("Green");
		blue = new JCheckBox("Blue");
		panel.add(red);
		panel.add(green);
		panel.add(blue);
		add(panel,BorderLayout.SOUTH);
	}
		

	class addActionListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			setColor();
		}

		private void setColor() {
			Color c = new Color(0,0,0);
			if(red.isSelected()){
				c = new Color(255,0,0);
				if(green.isSelected()){
					c = new Color(255,255,0);
					if(blue.isSelected()){
						c = new Color(255,255,255);
					}
				}
				else if(blue.isSelected()){
					c = new Color(255,0,255);
				}
			}
			else if(green.isSelected()){
				c = new Color(0,255,0);
				if(blue.isSelected()){
					c = new Color(0,255,255);
				}
			}
			else if(blue.isSelected()){
				c = new Color(0,0,255);
			}
			
			colorPanel.setBackground(c);
			
		}
	}
	
}
	


