package color;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;



public class Frame_ComboBox extends JFrame{
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 335;
	private JPanel colorPanel,panel;
	private JComboBox comboB;
	
	public static void main(String[] args){
		new Frame_ComboBox();
	}
		
	public Frame_ComboBox(){
		createPanel();
		setSize(FRAME_WIDTH,FRAME_HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setLocation(600,200);
		
		
	}
	private void createPanel() {
		colorPanel = new JPanel();
		colorPanel.setBackground(new Color(255,0,0));
		add(colorPanel,BorderLayout.CENTER);
		
		panel = new JPanel();
		comboB = new JComboBox();
		comboB.addItem("Red");
		comboB.addItem("Green");
		comboB.addItem("Blue");
		panel.add(comboB);
		
		
		comboB.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if(comboB.getSelectedItem().equals("Red")){
					colorPanel.setBackground(new Color(255,46,46));
				}
				else if(comboB.getSelectedItem().equals("Green")){
					colorPanel.setBackground(new Color(25,255,52));
				}
				else{
					colorPanel.setBackground(new Color(12,29,255));
				}
			}
		});
		add(panel,BorderLayout.SOUTH);

	
	}
}
	


