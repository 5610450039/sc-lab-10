package color;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;



public class Frame_Menu extends JFrame{
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 335;
	private JMenu mColor;
	private JMenuBar bar;
	private JPanel colorPanel;
	private JMenuItem red,green,blue;
	
	public static void main(String[] args){
		new Frame_Menu();
	}
		
	public Frame_Menu(){
		createPanel();
		setSize(FRAME_WIDTH,FRAME_HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setLocation(600,200);
		
		
	}
	private void createPanel() {
		colorPanel = new JPanel();
		colorPanel.setBackground(new Color(255,0,0));
		add(colorPanel,BorderLayout.CENTER);
		
		mColor = new JMenu("Color");
		red = new JMenuItem("Red");
		green = new JMenuItem("Green");
		blue = new JMenuItem("Blue");
		mColor.add(red);
		mColor.add(green);
		mColor.add(blue);
		bar = new JMenuBar();
		setJMenuBar(bar);
		bar.add(mColor);
		
		
		
		class redListener implements ActionListener{
			public void actionPerformed(ActionEvent e){
				colorPanel.setBackground(new Color(255,0,0));
			}
		}
		class greenListener implements ActionListener{
			public void actionPerformed(ActionEvent e){
				colorPanel.setBackground(new Color(0,255,0));
			}
		}
		class blueListener implements ActionListener{
			public void actionPerformed(ActionEvent e){
				colorPanel.setBackground(new Color(0,0,255));
			}
		}
	
		red.addActionListener(new redListener());
		green.addActionListener(new greenListener());
		blue.addActionListener(new blueListener());
		
	}
}
	


