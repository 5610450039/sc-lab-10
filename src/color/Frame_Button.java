package color;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;



public class Frame_Button extends JFrame{
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 335;
	private JPanel colorPanel,button;
	private JButton red,green,blue;
	
	public static void main(String[] args){
		new Frame_Button();
	}
		
	public Frame_Button(){
		setSize(FRAME_WIDTH,FRAME_HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setLocation(600,200);
		createPanel();
		
	}
	private void createPanel() {
		colorPanel = new JPanel();
		colorPanel.setBackground(new Color(255,0,0));
		add(colorPanel,BorderLayout.CENTER);
		
		button = new JPanel();
		red = new JButton("Red");
		green = new JButton("Green");
		blue = new JButton("Blue");
		button.add(red);
		button.add(green);
		button.add(blue);
		add(button,BorderLayout.SOUTH);
		
		class redListener implements ActionListener{
			public void actionPerformed(ActionEvent e){
				colorPanel.setBackground(new Color(255,0,0));
			}
		}
		class greenListener implements ActionListener{
			public void actionPerformed(ActionEvent e){
				colorPanel.setBackground(new Color(0,255,0));
			}
		}
		class blueListener implements ActionListener{
			public void actionPerformed(ActionEvent e){
				colorPanel.setBackground(new Color(0,0,255));
			}
		}
	
		red.addActionListener(new redListener());
		green.addActionListener(new greenListener());
		blue.addActionListener(new blueListener());
	}
}
	


